// const rps = (p1, p2) => {

//     if(p1 === p2) {
//         return "Draw!";
//     }else if(p1 === 'rock' && p2 === 'scissor' || p1 === 'scissor' && p2 === 'paper' || p1 === 'paper' && p2 === 'rock') {
//         return "Player 1 won!"
//     } else {
//         return "Player 2 won!"
//     }

// };

const rps = (p1, p2) => {

    const rules = {
        rock: 'scissor',
        scissor: 'paper',
        paper: 'rock'
    }

    if(p1 === p2) {
        return 'Draw';
    }else {
        return `Player ${(rules[p1] === p2) ? '1' : '2'} won!`
    }

};

console.log(rps('scissor', 'paper'))