const min = function(list){
    
    return list.reduce((acc, curr) => acc < curr ? acc : curr);
}

const max = function(list){
    
    return list.reduce((acc, curr) => acc > curr ? acc : curr);

}

console.log(Math.min(...[10,11,9,-2,-200,424,150]))

