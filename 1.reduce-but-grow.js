const arrayofInt = [1, 2, 3, 4];

function grow(x) {
    return x.map(int => int * int);
}

grow(arrayofInt);