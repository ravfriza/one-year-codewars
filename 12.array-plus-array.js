const arrayPlusArray = (arr1, arr2) => [...arr1, ...arr2].reduce((acc, curr) => acc + curr);

console.log(arrayPlusArray([2,4,6,2,7,4], [200,5623,1235,6,12,325,5]))