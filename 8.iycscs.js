const countSheep = num => {
    return [...Array(num)].map((_, i) => `${++i} sheep...`).join("")
}

console.log(countSheep(10));